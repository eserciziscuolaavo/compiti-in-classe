
package CompitoInClasse;

import java.util.Scanner;

public class File {
	String path = null;
	static Scanner input = new Scanner(System.in);

	public File(String path) {
		this.setPath(path);
	}
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		path = input.nextLine();
		this.path = path;
	}
}