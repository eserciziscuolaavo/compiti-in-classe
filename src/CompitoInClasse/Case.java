
package CompitoInClasse;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class Case {
	
	Map<Integer, String> mappa = new HashMap<Integer, String>();
	
	boolean errore = true;
	Scanner input = new Scanner(System.in);
	String risposta;
	String scelta;
	String value;
        String key;
        Integer valueKey;
	
	public void getCase1() {

				System.out.println("Aggiungi una nuova associazione alias-string!\n");
				System.out.println("Alias: ");
				String aliasAdd = input.nextLine();
				
				System.out.println("Inserisci il pathName: ");
				String pathNameAdd = input.nextLine();
				File file = new File(pathNameAdd);
				
				Mappa.put(aliasAdd, file);	
				
				System.out.println(file.getPath());
				

        }
//-------------------------------------------------------------------------------------------//		
	public void getCase2() {

				do {
					if (mappa.isEmpty()) {
						System.out.println("La mappa e vuota!\n");
						break;
					}
					try {
						System.out.println("Scegli l'alis-path da eliminare ");
		                                System.out.println("alias disponibili:" + mappa.keySet());
						risposta = (input.nextLine());
						key = (risposta);
						
						String check = mappa.remove(key); 
						
						if (check == null) {
							System.out.println("Non esiste nessun alias-path chiamata ["+key+"]");
							break;
						}
						
		                if (mappa.isEmpty()) {
		                	break;
		                }

						while (errore) {
							System.out.print("Vuoi eliminare altro dalla mappa ? (S/N) ");
							System.out.println("");
							risposta = input.nextLine().toUpperCase();
							if (risposta.equalsIgnoreCase("S") || risposta.equalsIgnoreCase("N"))
								break;
						}
						if (risposta.equalsIgnoreCase("N")) {
							break;
						}
						
					} catch (Exception e) {
						System.out.println("Errore!scelta effettuata è sbagliata");
					}
				} while (errore);
			
        }
//-------------------------------------------------------------------------------------------//	
	
	public void getCase3() {
		System.out.println("Elenco Mappa: \n");
				for (Integer i : mappa.keySet()) {
				      System.out.println(i + " associato a " + mappa.get(i));
				    }
	}
        public void getCase4() {
		if (!mappa.isEmpty()) {
			System.out.println("la mappa è vuota, non c'è nulla da eliminare!\n");
		} else {
			System.out.print("Eliminazione in corso");
			for (int i = 0; i < 3; i++) {
				try {
					Thread.sleep(800);
				} catch (Exception e) {
					System.out.println(e);
				}
				System.out.printf(".", (i + 1));
			}
		mappa.clear();
		try {
			Thread.sleep(1200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("\nEliminazione completata!\n");
	}
	}
        
        
      }
