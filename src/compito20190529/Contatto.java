
package compito20190529;

class Contatto implements Record{
    final static int Record_size=300;
    final static String marker="frenk";
    private String cognome;
    private String nome;
    private int numero;

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public int getRecordSize() {
    return 300;
    }
    
}

