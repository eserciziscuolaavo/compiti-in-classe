/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compito20190529;

import java.util.Scanner;

public class Tools {

    static String leggiStringa(String Prompt, Integer maxLenght) {
        Scanner reader = new Scanner(System.in);
        while (true) {
            System.out.print(Prompt);
            String risposta = reader.nextLine();
            if (maxLenght != null && risposta.length() > maxLenght) {
                System.out.println("La stringa non puo essere più lunga di " + maxLenght + " caratteri");
                continue;
            }
            return risposta;
        }
    }

    static int leggiIntero(String Prompt, Integer min, Integer max) {
        Scanner reader = new Scanner(System.in);
        while (true) {
            System.out.print(Prompt);
            String risposta = reader.nextLine();
            try {
                int numero = Integer.parseInt(risposta);
                if (max != null && numero > max) {
                    System.out.println("Il numero non puo essere maggiore di " + max + " cifre");
                    continue;
                }
                if (min != null && numero < min) {	// CORRETTO
                    System.out.println("Il numero non puo essere minore di " + min + " cifre");
                    continue;
                }
                return numero;
            } catch (NumberFormatException ex) {
                System.out.println("Devio inserire un numero intero");
            }

        }
    }

    static String leggiString(String Prompt) {
        Scanner reader = new Scanner(System.in);
        while(true){
            System.out.println(Prompt);
            String risposta = reader.nextLine();
            return risposta;
        }
    }

    static int rispostaNumero(String Prompt, Integer min, Integer max) {
    Scanner reader = new Scanner(System.in);
    max = 2;
    min = 0;
        while (true) {
            System.out.print(Prompt);
            String risposta = reader.nextLine();
            try {
                int numero = Integer.parseInt(risposta);
                if (max != null && numero > max) {
                    System.out.println("Il numero non puo essere maggiore di " + max + " cifre");
                    continue;
                }
                if (min != null && numero < min) {	// CORRETTO
                    System.out.println("Il numero non puo essere minore di " + min + " cifre");
                    continue;
                }
                return numero;
            } catch (NumberFormatException ex) {
                System.out.println("Devi inserire un numero intero");
            }

        }
        }
    }


